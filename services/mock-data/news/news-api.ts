import { NewsData } from "./new-data"

export function getNewsData() {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(NewsData);
        },300)
    })
}