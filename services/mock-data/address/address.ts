export const Address = [
    {
      name: "Head Office Hà Nội",
      hotline: "0983.930.475.",
      tel: "(024) 62557888.",
      address:"Tầng 3,4 Số 311 Trường Chinh, Q.Thanh Xuân, Hà Nội.",
    },
    {
      name: "Chi Nhánh Hồ Chí Minh",
      hotline: "0983.930.475.",
      tel: "(024) 62557888.",
      address:"120/21 đường 59, Q. Gò Vấp, HCM",
    },
  ];
  