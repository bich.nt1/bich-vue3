import { FooterMenu } from "./footer-menu";

export function getFooterMenu() {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(FooterMenu);
        },300)
    })
}