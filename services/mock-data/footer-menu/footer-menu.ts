export const FooterMenu = [
  {
    name: "Áo thun đồng phục",
  },
  {
    name: "Áo nhóm",
  },
  {
    name: "Áo đồng phục lớp",
  },
  {
    name: "Áo đồng phục họp lớp",
  },
  {
    name: "Áo lớp 3D Galaxy",
  },
  {
    name: "Áo đồng phục công ty",
  },
  {
    name: "Đồng phục Spa",
  },
  {
    name: "Áo cờ đỏ sao vàng",
  },
  {
    name: "Đồng phục đi biển",
  },
  {
    name: "Đồng phục công nhân",
  },
  {
    name: "Áo nhóm đi du lịch",
  },
  {
    name: "Áo khoác đồng phục",
  },
  {
    name: "Áo khoác gió đồng phục",
  },
  {
    name: "Áo khoác nỉ đồng phục",
  },
  {
    name: "Áo thun quảng cáo",
  },
  {
    name: "Đồng phục sự kiện",
  },
  {
    name: "Đồng phục ngân hàng",
  },
  {
    name: "Đồng phục nhân viên",
  },
  {
    name: "Đồng phục nhân viên nhà hàng",
  },
  {
    name: "Đồng phục PG",
  },
  {
    name: "Đồng phục quán cafe",
  },
];
